<?php

use App\Database\PDODatabaseConnection;
use App\Database\PDOQueryBuilder;
use App\Helpers\Config;

require_once "./vendor/autoload.php";

$config = Config::get('database', 'pdo_testing');
$pdoConnection = new PDODatabaseConnection($config);
$queryBuilder = new PDOQueryBuilder($pdoConnection->connect());


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $queryBuilder->table('bugs')
        ->create(request());
    jsonResponse();
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $queryBuilder->table('bugs')
        ->where('id', request()['id'])
        ->update(request());
    jsonResponse();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $data = $queryBuilder->table('bugs')
        ->find(request()['id']);
    jsonResponse($data);
}

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $queryBuilder->table('bugs')
        ->where('id', request()['id'])
        ->delete();
    jsonResponse(null, 204);
}