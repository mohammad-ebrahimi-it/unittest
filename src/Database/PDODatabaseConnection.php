<?php

namespace App\Database;

use App\Contracts\DatabaseConnectionInterface;
use App\Exceptions\ConfigNotValidException;
use App\Exceptions\DatabaseConnectionException;
use PDO;
use PDOException;

class PDODatabaseConnection implements DatabaseConnectionInterFace
{

    protected array $config;
    protected PDO $connection;

    const REQUIRED_CONFIG_KEYS = [
        'driver',
        'host',
        'database',
        'db_user',
        'db_password'
    ];

    /**
     * @throws ConfigNotValidException
     */
    public function __construct(array $config)
    {
        if (!$this->isConfigValid($config)) {
            throw new ConfigNotValidException();
        }
        $this->config = $config;
    }

    /**
     * @throws DatabaseConnectionException
     */
    public function connect(): static
    {
        $dsn = $this->generateDsn($this->config);
        try {
            $this->connection = new PDO(...$dsn);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (PDOException $exception) {
            throw new DatabaseConnectionException($exception->getMessage());
        }
        return $this;
    }

    public function getConnect(): PDO
    {
        return $this->connection;
    }

    private function generateDsn(array $config): array
    {
        $dsn = "{$config['driver']}:host={$config['host']};dbname={$config['database']};";
        return [$dsn, $config['db_user'], $config['db_password']];
    }

    private function isConfigValid(array $config): bool
    {
        $matches = array_intersect(self::REQUIRED_CONFIG_KEYS, array_keys($config));
        return count($matches) === count(self::REQUIRED_CONFIG_KEYS);
    }
}