<?php

namespace App\Database;

use App\Contracts\DatabaseConnectionInterface;
use PDO;

class PDOQueryBuilder
{
    protected object $connection;
    protected string $table;
    protected $conditions;
    protected array $values;
    private $statement;

    public function __construct(DatabaseConnectionInterface $connection)
    {
        $this->connection = $connection->getConnect();
    }

    public function table(string $table): static
    {
        $this->table = $table;
        return $this;
    }

    public function create(array $data): int
    {
        $placeholder = [];
        foreach ($data as $column => $value) {
            $placeholder [] = "?";
        }
        $fields = implode(",", array_keys($data));
        $value = implode(",", $placeholder);
        $this->values = array_values($data);
        $sql = "INSERT INTO $this->table ($fields) VALUES ($value)";
        $this->execute($sql);
        return (int)$this->connection->lastInsertId();
    }

    public function where(string $column, string $value): static
    {
        if (is_null($this->conditions))
            $this->conditions = "$column = ?";
        else
            $this->conditions .= " and $column=?";
        $this->values[] = $value;
        return $this;
    }

    public function update(array $data)
    {
        $fields = [];
        foreach ($data as $column => $value) {
            $fields[] = "$column='$value'";
        }
        $fields = implode(',', $fields);
        $sql = "UPDATE $this->table SET $fields WHERE $this->conditions";
        $this->execute($sql);
        return $this->statement->rowCount();
    }

    public function truncateAllTable(): void
    {
        $query = $this->connection->prepare("show tables");
        $query->execute();
        foreach ($query->fetchAll(PDO::FETCH_COLUMN) as $table) {
            $this->connection->prepare("TRUNCATE TABLE `$table`")->execute();
        }
    }

    public function delete()
    {
        $sql = "DELETE FROM $this->table WHERE $this->conditions";
        $this->execute($sql);
        return $this->statement->rowCount();
    }

    public function get(array $columns = ['*'])
    {
        $columns = implode(',', $columns);
        $sql = "SELECT $columns FROM $this->table WHERE ($this->conditions)";
        $this->execute($sql);
        return $this->statement->fetchAll();
    }

    public function first(array $columns = ['*'])
    {
        $data = $this->get($columns);
        return empty($data) ? null : $data[0];
    }

    public function find(int $id)
    {
        return $this->where('id', $id)->first();
    }

    public function findBy(string $column, string $value)
    {
        return $this->where($column, $value)->first();
    }

    public function beginTransaction(): void
    {
        $this->connection->beginTransaction();
    }

    public function rollback(): void
    {
        $this->connection->rollback();
    }

    private function execute(string $sql): static
    {
        $this->statement = $this->connection->prepare($sql);
        $this->statement->execute($this->values);
        $this->values = [];
        return $this;
    }


}