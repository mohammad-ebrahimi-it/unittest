<?php

function jsonResponse($data = null, $statusCode = 200)
{
    header_remove();
    header('Content-type: application/json');
    http_response_code($statusCode);
    echo json_encode($data);
    exit();
}
function request() {
    return json_decode(file_get_contents('php://input'), true);
}