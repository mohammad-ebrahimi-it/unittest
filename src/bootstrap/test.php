<?php

use App\Database\PDODatabaseConnection;
use App\Database\PDOQueryBuilder;
use App\Exceptions\ConfigFileNotFoundException;
use App\Exceptions\ConfigNotValidException;
use App\Exceptions\DatabaseConnectionException;
use App\Helpers\Config;

require_once __DIR__ . "/../../vendor/autoload.php";

try {
    $config = Config::get('database', 'pdo_testing');
} catch (ConfigFileNotFoundException $e) {
    echo $e->getMessage();
}
/**
 * @var Config $config
 */
try {
    $pdoConnection = new PDODatabaseConnection((array)$config);
} catch (ConfigNotValidException $e) {
    echo $e->getMessage();
}

/** @var PDODatabaseConnection $pdoConnection */
try {
    (new PDOQueryBuilder($pdoConnection->connect()))->truncateAllTable();
} catch (DatabaseConnectionException $e) {
    echo $e->getMessage();
}
