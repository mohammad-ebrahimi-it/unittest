<?php

namespace Tests\Functional;

use App\Database\PDODatabaseConnection;
use App\Database\PDOQueryBuilder;
use App\Exceptions\ConfigNotValidException;
use App\Exceptions\DatabaseConnectionException;
use App\Helpers\Config;
use App\Helpers\HttpClient;
use PHPUnit\Framework\TestCase;

class CrudTest extends TestCase
{
    private PDOQueryBuilder $PDOQueryBuilder;
    private $httpClient;

    /**
     * @throws ConfigNotValidException
     * @throws DatabaseConnectionException
     */
    protected function setUp(): void
    {
        $pdoConnection = new PDODatabaseConnection($this->getConfig());
        $this->PDOQueryBuilder = new PDOQueryBuilder($pdoConnection->connect());
        $this->httpClient = new HttpClient();
        parent::setUp();
    }

    public function tearDown(): void
    {
        $this->httpClient = null;
        parent::tearDown();
    }

    public function testItCanCreateDataWithApi()
    {
        $data = [
            'json' => [
                'name' => 'api',
                'user' => 'ebi',
                'email' => 'e@gmail.com',
                'link' => 'https://link.com',
            ]
        ];

        $response = $this->httpClient->post('index.php', $data);
//       echo $response->getBody();
        $this->assertEquals(200, $response->getStatusCode());

        $bug = $this->PDOQueryBuilder
            ->table('bugs')
            ->where('user', 'ebi')
            ->where('name', 'api')
            ->first();
        $this->assertNotNull($bug);
        return $bug;
    }

    /**
     * @depends testItCanCreateDataWithApi
     */
    public function testItCatUpdateDataWithApi($bug)
    {
        $data = [
            'json' => [
                'id' => $bug->id,
                'name' => 'update'
            ]
        ];
        $response = $this->httpClient->put('index.php', $data);
        $this->assertEquals(200, $response->getStatusCode());
        $bug = $this->PDOQueryBuilder
            ->table('bugs')
            ->find($bug->id);
        $this->assertNotNull($bug);
        $this->assertEquals('update', $bug->name);
    }

    /**
     * @depends testItCanCreateDataWithApi
     */
    public function testItCanFetchDataWithApi($bug)
    {
        $response = $this->httpClient->get('index.php', ['json' => ['id' => $bug->id]]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('id', json_decode($response->getBody(), true));
    }

    /**
     * @depends testItCanCreateDataWithApi
     */
    public function testItCanDeleteWithApi($bug)
    {
        $response = $this->httpClient->delete('index.php', [
            'json' => [
                'id' => $bug->id
            ]
        ]);

        $this->assertEquals(204, $response->getStatusCode());
        $bugs = $this->PDOQueryBuilder
            ->table('bugs')
            ->find($bug->id);
        $this->assertNull($bugs);
    }


    private function getConfig()
    {
        return Config::get('database', 'pdo_testing');
    }
}