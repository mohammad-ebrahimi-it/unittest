<?php

namespace Tests\Unit;

use App\Exceptions\ConfigFileNotFoundException;
use App\Helpers\Config;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{

    public function testGetFileContentsReturnArray()
    {
        $config = Config::get('database');
        $this->assertIsArray($config);
    }

    public function testItThrowExceptionIfFileNotFound()
    {
        $this->expectException(ConfigFileNotFoundException::class);
        Config::getFileContents('dummy');
    }

    public function testGetMethodReturnsValidData()
    {
        $config =  Config::get('database', 'pdo');

        $expectedData = [
                'driver' => 'mysql',
                'host' => '127.0.0.1',
                'database' => 'bug_tracker',
                'db_user' => 'root',
                'db_password' => '851074750122'
        ];
        $this->assertEquals($config, $expectedData);


    }
}