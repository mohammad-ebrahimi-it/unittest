<?php

namespace Tests\Unit;

use App\Contracts\DatabaseConnectionInterface;
use App\Database\PDODatabaseConnection;
use App\Exceptions\{
    ConfigFileNotFoundException,
    ConfigNotValidException,
    DatabaseConnectionException
};
use App\Helpers\Config;
use PDO;
use PHPUnit\Framework\TestCase;

class PDODatabaseConnectionTest extends TestCase
{
    /**
     * @throws ConfigFileNotFoundException
     * @throws ConfigNotValidException
     */
    public function testPDoDatabaseConnectionImplementDatabaseConnectionInterface()
    {
        $config = $this->getConfig();
        $pdoConnection = new PDODatabaseConnection($config);
        $this->assertInstanceOf(DatabaseConnectionInterface::class, $pdoConnection);
    }

    /**
     * @throws DatabaseConnectionException|ConfigFileNotFoundException|ConfigNotValidException
     */
    public function testConnectMethodShouldReturnValidInstance(): PDODatabaseConnection
    {
        $config = $this->getConfig();
        $pdoConnection = new PDODatabaseConnection($config);
        $pdoHandler = $pdoConnection->connect();
        $this->assertInstanceOf(PDODatabaseConnection::class, $pdoHandler);
        return $pdoHandler;
    }

    /**
     * @depends testConnectMethodShouldReturnValidInstance
     */
    public function testConnectMethodShouldBeConnectToDatabase(PDODatabaseConnection $pdoConnection)
    {
        $this->assertInstanceOf(PDO::class, $pdoConnection->getConnect());
    }

    /**
     * @throws ConfigFileNotFoundException
     * @throws ConfigNotValidException
     */
    public function testItThrowExceptionConfigIsInvalid()
    {
        $this->expectException(DatabaseConnectionException::class);
        $config = $this->getConfig();
        $config['database'] = 'dummy';
        $pdoConnection = new PDODatabaseConnection($config);
        $pdoConnection->connect();
    }

    /**
     * @throws ConfigNotValidException
     * @throws DatabaseConnectionException
     * @throws ConfigFileNotFoundException
     */
    public function testReceivedConfigHaveRequiredKey()
    {
        $this->expectException(ConfigNotValidException::class);
        $config = $this->getConfig();
        unset($config['db_user']);
        $pdoConnection = new PDODatabaseConnection($config);
        $pdoConnection->connect();

    }

    /**
     * @throws ConfigFileNotFoundException
     */
    private function getConfig()
    {
        return Config::get('database', 'pdo_testing');
    }
}