<?php

namespace Tests\Unit;

use App\Database\PDODatabaseConnection;
use App\Database\PDOQueryBuilder;
use App\Exceptions\ConfigFileNotFoundException;
use App\Exceptions\ConfigNotValidException;
use App\Exceptions\DatabaseConnectionException;
use App\Helpers\Config;
use PHPUnit\Framework\TestCase;

class PDOQueryBuilderTest extends TestCase
{
    private object $queryBuilder;

    /**
     * @throws ConfigNotValidException
     * @throws ConfigFileNotFoundException
     * @throws DatabaseConnectionException
     */
    protected function setUp(): void
    {
        $config = $this->getConfig();
        $pdoConnection = new PDODatabaseConnection($config);
        $this->queryBuilder = new PDOQueryBuilder($pdoConnection->connect());
        $this->queryBuilder->beginTransaction();
        parent::setUp();
    }

    public function testItCanCreateData()
    {
        $result = $this->insertIntoDB();
        $this->assertIsInt($result);
        $this->assertGreaterThan(0, $result);

    }

    public function testItCanUpdateData()
    {
        $this->insertIntoDB();
        $result = $this->queryBuilder->table('bugs')
            ->where('user', 'mohammad')
            ->update(['email' => 'mamad@gmail.com', 'name' => 'ebi']);
        $this->assertEquals(1, $result);
    }

    public function testItCanUpdateWithMultipleWhere()
    {
        $this->insertIntoDB();
        $this->insertIntoDB(['user' => 'ali ebrahimi']);
        $result = $this->queryBuilder
            ->table('bugs')
            ->where('user', 'mohammad')
            ->where('link' , 'https://link.com')
            ->update(['name' => 'after multiple where']);
        $this->assertEquals(1, $result);

    }

    public function testItCanDeleteRecord()
    {
        $this->insertIntoDB();
        $this->insertIntoDB();
        $this->insertIntoDB();
        $this->insertIntoDB();
        $result = $this->queryBuilder->table('bugs')
            ->where('user', 'mohammad')
            ->delete();
        $this->assertEquals(4, $result);
    }

    public function testItCanFetchData()
    {
        $this->multipleInsertIntoDB(10, ['user' => 'morteza ahmadi']);
        $result = $this->queryBuilder
            ->table('bugs')
            ->where('user', 'morteza ahmadi')
            ->get();
        $this->assertIsArray($result);
        $this->assertCount(10, $result);
    }


    public function testInCanFetchSpecificColumns()
    {
        $this->multipleInsertIntoDB(10, ['name' => 'New']);
        $result = $this->queryBuilder
            ->table('bugs')
            ->where('name', 'New')
            ->get(['name', 'user']);
        $this->assertIsArray($result);
        $this->assertObjectHasAttribute('name', $result[0]);
        $this->assertObjectHasAttribute('user', $result[0]);
        $result = json_decode(json_encode($result[0]), true);
        $this->assertEquals(['name', 'user'], array_keys($result));
    }

    public function testItCanGetFirstRow()
    {
        $this->multipleInsertIntoDB(10, ['name' => 'first row']);
        $result = $this->queryBuilder
            ->table('bugs')
            ->where('name' , 'first row')
            ->first();
        $this->assertIsObject($result);
        $this->assertObjectHasAttribute('id', $result);
        $this->assertObjectHasAttribute('email', $result);
        $this->assertObjectHasAttribute('name', $result);
        $this->assertObjectHasAttribute('link', $result);
        $this->assertObjectHasAttribute('user', $result);
    }

    public function testItCanFindWithID()
    {
        $this->insertIntoDB();
        $id =  $this->insertIntoDB(['name' => 'for find']);
        $result = $this->queryBuilder
            ->table('bugs')
            ->find($id);
        $this->assertIsObject($result);
        $this->assertEquals('for find', $result->name);
    }

    public function testItCanFindBy()
    {
        $this->insertIntoDB();
        $id =  $this->insertIntoDB(['name' => 'for find by']);
        $result = $this->queryBuilder
            ->table('bugs')
            ->findBy('name', 'for find by');
        $this->assertIsObject($result);
        $this->assertEquals($id, $result->id);
    }

    public function testItReturnEmptyArrayWhenRecordNotFound()
    {
        $result = $this->queryBuilder
            ->table('bugs')
            ->where('user', 'dummy')
            ->get();
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    public function testItReturnNullWhenFirstRecordFound()
    {
        $result = $this->queryBuilder
            ->table('bugs')
            ->where('user', 'dummy')
            ->first();
        $this->assertNull($result);
    }

    public function testItReturnsZeroWhenRecordNotFoundUpdate()
    {
        $this->multipleInsertIntoDB(4);
        $result = $this->queryBuilder
            ->table('bugs')
            ->where('user', 'dummy')
            ->update(['name' => 'test']);
        $this->assertEquals(0, $result);
    }

    private function multipleInsertIntoDB(int $count,  $options = [])
    {
        for ($i = 1; $i<= $count; $i++) {
            $this->insertIntoDB($options);
        }
    }

    /**
     * @throws ConfigFileNotFoundException
     */
    private function getConfig()
    {
        return Config::get('database', 'pdo_testing');
    }

    private function insertIntoDB($options = []): int
    {
        $data = array_merge([
            'name' => 'first bug report',
            'link' => 'https://link.com',
            'user' => 'mohammad',
            'email' => 'm@gmail.com'
        ], $options);
        return $this->queryBuilder->table('bugs')->create($data);
    }

    protected function tearDown(): void
    {
//        $this->queryBuilder->truncateAllTable();
        $this->queryBuilder->rollback();
        parent::tearDown();
    }

}